import React, {Fragment, useState, useEffect} from 'react';
import Formulario from './components/Formulario';
import Cita from './components/Cita';

function App() {

  // citas en local storage
  let citasIniciales =  JSON.parse(localStorage.getItem('citas'));

  if (!citasIniciales) {
    citasIniciales = [];
  }

  //citasGeneradas
  const [citas, setCitas] = useState(citasIniciales);

  // useEffect operaciones cuando el state cambia en este caso citas  
  useEffect(() => {
    if (citasIniciales) {
      localStorage.setItem('citas', JSON.stringify(citas));
    }else{
      localStorage.setItem('citas', JSON.stringify([]));
    }
    
  }, [citas,citasIniciales])
  

  // agregar cita tambien se pudo haber pasado el citas y setcitas y agregarlas en el hijo de la misma forma
  const addCita = cita => {
    
    setCitas(
      [
        ...citas,
        cita
      ]
    )

  }

  const eliminarCita = id => {
    const nuevaCita = citas.filter(cita => cita.id !== id )
    setCitas(nuevaCita)
    
  }

  const titulo = (citas.length === 0) ? "No hay citas" : "Citas Agregadas" ;

  return (
    <Fragment>
      <h1>Medical</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario
              //setCitas = {setCitas}
              //cita {cita}
              addCita = {addCita}
            />
          </div>

          <div className="one-half column">
            <h2> {titulo} </h2>
            {citas.map(cita =>(
              <Cita
              key = {cita.id}
              cita = {cita}
              eliminarCita = {eliminarCita}
              />
            ))}
          </div>
        </div>
      </div>
      
    </Fragment>
  );
}



export default App;
