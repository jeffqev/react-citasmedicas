import React,{Fragment, useState} from 'react'
import uuid from 'uuid/dist/v4'
import PropTypes from 'prop-types';

const Formulario = ({addCita}) => {

    // useState
    const [cita, setCita] = useState({
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: '',
    })

    const [errores, setErrores] = useState(false)

    //Funciones
    const actualizarForm = e => {
        setCita(
            {
                ...cita,
                [e.target.name] : e.target.value,
            }
        ) 
        
    }

    
    const {mascota, propietario, fecha,hora,sintomas} = cita
    
    const submitCita = e =>{

        e.preventDefault()
        //Validar 

        if (mascota.trim()==='' || propietario.trim()==='' || fecha.trim()==='' || hora.trim()==='' || sintomas.trim()==='') {
            setErrores(true);

            return;
            
        }
        setErrores(false)
        
        // ID
        cita.id = uuid();
        
        
        // Crear cita viene con el id generado y va al componente padre para poder pasarlo a otro componente hijo
        addCita(cita);


        // Reiniciar form

        setCita({
            mascota: '',
            propietario: '',
            fecha: '',
            hora: '',
            sintomas: ''
        })
        
    }
    //-------------------------------------------------

    

    return ( 
        <Fragment>
            <h2>Nueva cita</h2>
            {errores ? <p className="alerta-error"> Todos los campos son obligatorios</p> : null}
            <form
                onSubmit={submitCita}
            >
                <label htmlFor=""> Nombre Mascota</label>
                <input 
                type="text"
                name="mascota"
                className="u-full-width"
                placeholder = "Nombre mascota"
                onChange = {actualizarForm}
                value={mascota}
                />

                <label htmlFor=""> Nombre Dueño</label>
                <input 
                type="text"
                name="propietario"
                className="u-full-width"
                placeholder = "Nombre Dueño"
                onChange = {actualizarForm}
                value={propietario}
                />

                <label htmlFor=""> Fecha</label>
                <input 
                type="Date"
                name="fecha"
                className="u-full-width"
                onChange = {actualizarForm}
                value={fecha}
                />

                <label htmlFor=""> Hora</label>
                <input 
                type="time"
                name="hora"
                className="u-full-width"
                onChange = {actualizarForm}
                value={hora}
                />

                <label htmlFor=""> Sintomas</label>
                <textarea 
                name="sintomas" 
                className="u-full-width"
                onChange = {actualizarForm}
                value={sintomas}
                >

                </textarea>

                <button
                    type="submit"
                    className="u-full-width button-primary"
                >
                    Agregar Cita
                </button>

            </form>
        </Fragment>
     );
}

Formulario.propTypes ={
    addCita: PropTypes.func.isRequired,
    
  }
export default Formulario;